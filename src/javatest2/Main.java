//ganesh -748497
package javatest2;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int choice = 0;
		
		while (choice != 3) {
			// 1. show the menu
			showMenu();
			
			Shape s10=new Square(10, 10, 20);
			s10.calculateArea();
			
			Shape t1=new Triangle(10, 10, 10); 
			t1.calculateArea();
			// 2. get the user input
			System.out.println("Enter a number: ");
			choice = keyboard.nextInt();
			
			// 3. DEBUG: Output what the user typed in 
			System.out.println("You entered: " + choice);
			System.out.println();
			if(choice==1)
			{
				
				ArrayList<Shape> ShapeList = new ArrayList<>();
				ShapeList.add(t1);
	            
			}
			if(choice==2)
			{
				ArrayList<Shape>ShapeList =new ArrayList<>();
				ShapeList.add(s10);
				
			}
			if(choice==3)
			{
			    s10.printInfo();
			     t1.printInfo();
			}
		}}
	
	public static void showMenu() {
		System.out.println("AREA GENERATOR");
		System.out.println("==============");
		System.out.println("1. Triangle");
		System.out.println("2. Square");
		System.out.println("3. Exit");
	}

}
