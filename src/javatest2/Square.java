package javatest2;

public class Square extends Shape implements TwoDimensionalShapeInterface {
private double area1=1;
	Square(double b, double s, double h) {
		super(b, s, h);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double calculateArea() {
		// TODO Auto-generated method stub
	 area1=getSide()*getSide();
		return area1;
	}

	@Override
	public void printInfo() {
		// TODO Auto-generated method stub
		System.out.println("The area of the square is " +area1);
	}

}
